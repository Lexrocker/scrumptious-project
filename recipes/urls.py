from django.urls import path

from recipes.views import (
    #create_recipe, replaced below
    RecipieCreateView,
    #change_recipe,
    RecipeUpdateView,
    log_rating,
    #show_recipe, replaced below
    RecipeDetailView,
    #show_recipes,
    RecipeListView, # importing a class is the same
    RecipeDeleteView,
)

urlpatterns = [
    
    # path("", show_recipes, name="recipes_list"),  # replaced
    path("", RecipeListView.as_view(), name="recipes_list"), # when using a class and not a function, we need to use the .as_view method
    # path("<int:pk>/", show_recipe, name="recipe_detail"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    
    
    #path("new/", create_recipe, name="recipe_new"), replaced below
    path("new/", RecipieCreateView.as_view(), name="recipe_new"),
    
    
    #path("edit/", change_recipe, name="recipe_edit"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),


    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
]
