from django.shortcuts import redirect, render

from recipes.forms import RatingForm

from django.urls import reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

try:
    from recipes.forms import RecipeForm
    from recipes.models import Recipe
except Exception:
    RecipeForm = None
    Recipe = None


# def create_recipe(request):
#     if request.method == "POST" and RecipeForm:
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save()
#             return redirect("recipe_detail", pk=recipe.pk)
#     elif RecipeForm:
#         form = RecipeForm()
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/new.html", context)


class RecipieCreateView(CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name","author","description","image"]
    success_url = reverse_lazy("recipes_list")  # returns to the recipes_list URL path


# def change_recipe(request, pk):
#     if Recipe and RecipeForm:
#         instance = Recipe.objects.get(pk=pk)
#         if request.method == "POST":
#             form = RecipeForm(request.POST, instance=instance)
#             if form.is_valid():
#                 form.save()
#                 return redirect("recipe_detail", pk=pk)
#         else:
#             form = RecipeForm(instance=instance)
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/edit.html", context)

class RecipeUpdateView(UpdateView):
    model = Recipe
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")
    # success_url = "/" this also worked for some reason



# def show_recipes(request):
#     context = {
#         "recipes": Recipe.objects.all() if Recipe else [],
#     }
#     return render(request, "recipes/list.html", context)

class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


    # def get_queryset(self):
    #     # get the querystring from search
    #     querystring = self.request.GET.get("q")
    #     # generate list of recipes
    #     if querystring == None:
    #         querystring = ""
    #     return Recipe.objects.filter(description__icontains = querystring)
    #     # filter the list based on string we get
    #     # return the filtered list



# def show_recipe(request, pk):
#     context = {
#         "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
#         "rating_form": RatingForm(),  # highlight
#     }
#     return render(request, "recipes/detail.html", context)

class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs): # we are ovverriding how DetailView gets its context to add in the rating form
        # ^^  **kwargs allows you to handle named arguments that you have not defined in advance.
        # get_context_data is a Detailview Speific Django funciton for adding context to view classes utilizing DetailView
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context
        

def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
